﻿using UnityEngine;
using System.Collections;
using System;

//By marking this class Serializable, we will be able to modify it inside of our inspector
//if we have a public on another MonoBehavior of this type
[Serializable]
public class ControllerParameters2D 
{

	public enum JumpBehavior
	{
		CanJumpOnGround,
		CanJumpAnywhere,
		CantJump
	}

	//it is a 2D game so Vector3 isn't really necessary in this game because the z value is ignored
	public Vector2 MaxVelocity = new Vector2(float.MaxValue, float.MaxValue);

	//In the inspector, SlopeLimit will be represented as a slider between the values of 0 & 90
	[Range(0, 90)]
	//Steepest angle that the player can climb is 30 degrees
	public float SlopeLimit = 30;

	//By default, the universal force in the y direction is -25 units per sec 
	public float Gravity = -25f;

	public JumpBehavior JumpRestrictions;

	//this limits how often the user can jump if they CanJumpAnywhere
	public float JumpFrequency = .25f;

	public float JumpMagnitude = 12;
}
