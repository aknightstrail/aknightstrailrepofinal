﻿using UnityEngine;
using System.Collections;

public class camera : MonoBehaviour {



    public GameObject player;
    private Vector3 offset;

    // Use this for initialization
    void Start()
    {
        offset = transform.position;
    }



    // Update is called once per frame
    void Update()
    {
        transform.position = new Vector3(offset.x+player.transform.position.x, offset.y,offset.z);
    }
}

