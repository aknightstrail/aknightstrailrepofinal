﻿using UnityEngine.SceneManagement;
using UnityEngine;

class StartScreen : MonoBehaviour
{
    public string Level;

    public void Update()
    {
        if (Input.GetKeyDown(KeyCode.DownArrow))
            SceneManager.LoadScene(Level);
    }
}
